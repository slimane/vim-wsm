let s:lines = WSM#util#linsKey
let s:columsn = WSM#util#columnsKey
let s:recordFile = WSM#util#recordFile

function! WSM#lazy#saveCurrentWindowSize(sizeName)
    let g:windowSizeDict[a:sizeName] = {s:columsn : &columns
    \                                   , s:lines : &lines}
endfunction

function! WSM#lazy#setWindowSize()
    if !WSM#util#getYorN('Windowサイズを保存しますか?')
        return
    endif
    let l:sizeName = ''
    while 1
        let l:sizeName = input('Size名を入力してください(半角英数のみ) : ')
        if l:sizeName ==? '' || l:sizeName ==? '_'
            continu
        endif
        if match(l:sizeName, '\W') !=? -1
            continue
        endif
        if index(keys(g:windowSizeDict), l:sizeName) ==? -1
            break
        endif
        if WSM#util#getYorN(
        \               '同じSize名がすでに存在しますが上書きしますか?')
            break
        endif
    endwhile
    call WSM#lazy#saveCurrentWindowSize(l:sizeName)
endfunction


function! WSM#lazy#removeWindowSizeDictKey(sizeName)
    if !has_key(g:windowSizeDict, a:sizeName)
        EchoWarning 'invild arguments'
        return 0
    endif
    call remove(g:windowSizeDict, a:sizeName)
endfunction

function! WSM#lazy#changeWindowSize(sizeName)
    if !has_key(g:windowSizeDict, a:sizeName)
        EchoWarning 'invild arguments'
        return 0
    endif
    let &columns = g:windowSizeDict[a:sizeName][s:columsn]
    let &lines   = g:windowSizeDict[a:sizeName][s:lines]
endfunction

function! WSM#lazy#getWindowSizeList(A, C, L)
    let l:list = keys(g:windowSizeDict)
    echo l:list
    if a:A !=? ''
        return filter(deepcopy(l:list)
        \               , "match(v:val, a:A) ==? 0")
    endif

    return l:list
endfunction


function! WSM#lazy#writeWindowSize()
    call writefile(['let g:windowSizeDict = ' . string(g:windowSizeDict)], expand(s:recordFile))
    vertical new
    \ | execute 'silent edit! ' . s:recordFile
    \ | silent %s/}\zs\ze/\r\t\\ /ge
    \ | silent %s/\s=\s\zs\ze/\r\t\\ /e
    \ | silent exit
endfunction

function! WSM#lazy#getWinpos()
    redir => l:winpos | silent call s:returnWinpos() | redir end
    let l:winpos = matchstr(l:winpos, '.*:\zs.*\ze')
    let l:X = matchstr(l:winpos, '\sX\s*\zs\d\+\ze')
    let l:Y = matchstr(l:winpos, ',\s*Y\s*\zs\d\+\ze')
    execute 'redir >> ' . s:recordFile  | echo 'winpos ' . l:X . ' ' . l:Y  | redir end
endfunction

function! s:returnWinpos()
    winpos
endfunction
