let s:lines = WSM#util#linsKey
let s:columsn = WSM#util#columnsKey
let s:recordFile = WSM#util#recordFile

function! WSM#base#init()
    if filereadable(expand(s:recordFile))
        execute 'silent source ' . expand(s:recordFile)
    endif
    if !exists('g:windowSizeDict')
        let g:windowSizeDict = {'_' :
                    \ { s:columsn : &columns
                    \   , s:lines : &lines }}
    endif
    call WSM#base#changeWindowSize('_')
endfunction

function! WSM#base#changeWindowSize(sizeName)
    if !has_key(g:windowSizeDict, a:sizeName)
        WSM#util#WarningMsg('invild argument')
        return 0
    endif
    let &columns = g:windowSizeDict[a:sizeName][s:columsn]
    let &lines   = g:windowSizeDict[a:sizeName][s:lines]
endfunction
