let s:recordFile = WSM#util#recordFile

function! WSM#exit#writeWindowSize()
    call writefile(['let g:windowSizeDict = ' . string(g:windowSizeDict)], expand(s:recordFile))

    vertical new
    \ | execute 'silent edit! ' . s:recordFile
    \ | silent %s/}\zs\ze/\r\t\\ /ge
    \ | silent %s/\s=\s\zs\ze/\r\t\\ /e
    \ | silent exit
endfunction

function! WSM#exit#getWinpos()
    if exists(':winpos') !=? 2
        return
    endif
    redir =>  l:winpos
    \   | call WSM#util#silentCommand("winpos")
    \   | redir end
    let l:winpos = matchstr(l:winpos, '.*:\zs.*\ze')
    let l:X = matchstr(l:winpos, '\sX\s*\zs\d\+\ze')
    let l:Y = matchstr(l:winpos, ',\s*Y\s*\zs\d\+\ze')
    execute 'redir >> ' . s:recordFile
    \   | call WSM#util#silentEcho('winpos ' . l:X . ' ' . l:Y)
    \   | redir end
endfunction

