let WSM#util#linsKey = 'lines'
let WSM#util#columnsKey = 'columns'
let WSM#util#recordFile =
\   exists('g:WSMRecordFile') && filereadable(g:WSMRecordFile)
\       ? g:WSMRecordFile
\       :  '~/.WSM'

function! WSM#util#WarningMsg(message)
    echohl WarningMsg | echo a:message | echohl None
endfunction

function! WSM#util#getYorN(message)
    while 1
        let l:userInput = input(a:message . '[y/n] : ')
        if l:userInput  ==? 'n'
            return 0
        endif
        if l:userInput ==? 'y'
            return 1
        endif
    endwhile
endfunction


function! WSM#util#silentCommand(command)
    silent call s:executeCommand(a:command)
endfunction

function! s:executeCommand(command)
    execute a:command
endfunction

function! WSM#util#silentEcho(message)
    silent call s:executeEcho(a:message)
endfunction

function! s:executeEcho(message)
    echo a:message
endfunction
