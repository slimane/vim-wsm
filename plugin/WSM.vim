if has('gui_running')
    augroup WSM.vim
        autocmd!
        autocmd GUIEnter       * call WSM#base#init()

        autocmd VimLeavePre    * call WSM#lazy#saveCurrentWindowSize('_')
        autocmd VimLeavePre    * call WSM#exit#writeWindowSize()
        autocmd VimLeavePre    * call WSM#exit#getWinpos()
    augroup END


    command! -nargs=1 -complete=customlist,WSM#lazy#getWindowSizeList WSMRemoveKey
    \   :call WSM#lazy#removeWindowSizeDictKey("<args>")

    command! -nargs=0 WSMSet
    \   :call WSM#lazy#setWindowSize()

    command! -nargs=1 -complete=customlist,WSM#lazy#getWindowSizeList WSMChange
    \                           call WSM#base#changeWindowSize("<args>")
endif
